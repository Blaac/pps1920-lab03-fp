package u03

import u02.Modules.Person
import u02.Modules.Person._
import u03.Lists.List.Cons
import u03.Lists._

object PersonList {

  def getCourses(people: List[Person]) : List[String]= {
    List.flatMap(people)( person => person match {
      case teacher: Teacher => Cons(teacher.course,List.Nil())
      case _ => List.Nil()
    })
  }

}

object test extends App{
  println(PersonList.getCourses(Cons(Student("carlo",1),Cons(Teacher("Pippo","ciasdasdsdsa"),List.Nil()))))
}
