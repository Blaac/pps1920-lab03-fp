package u03

import u03.Lists.List.{Cons, Nil}

object Lists {

  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {
    case class Cons[E](head: E, tail: List[E]) extends List[E]
    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    def map[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()
    }

    def filter[A](l1: List[A])(pred: A=>Boolean): List[A] = l1 match {
      case Cons(h,t) if (pred(h)) => Cons(h, filter(t)(pred))
      case Cons(_,t) => filter(t)(pred)
      case Nil() => Nil()
    }

    def drop[A](l: List[A], n: Int): List[A] = (l,n) match {
      case (Cons(_,l),n)  if  n > 0 => drop(l,n-1)
      case _ => l
    }

    def flatMap[A,B](l: List[A])(f: A => List[B]): List[B] = l match {
      case Cons(h,t) => append(f(h),flatMap(t)(f))
      case Nil() => Nil()

    }

    def flatMapMap[A,B](l1: List[A])(mapper: A=>List[B]): List[B] = flatMap[A, B](l1) ({
      case e  => mapper(e)
      case _ => Nil[B]()
    })
    def flatMapFilter[A](l1: List[A])(pred: A=>Boolean): List[A] = flatMap[A, A](l1) ({
      case e if pred(e) => Cons(e, Nil())
      case _ => Nil[A]()
    })

    def max(l: List[Int]): Option[Int] = l match {
      case Cons(h,Nil()) => Some(h)
      case Cons(h,t)=> Some(Math.max(h,max(t).get))
      case Nil() => None
    }

    def foldLeft[A,B](l: List[A])(acc: B)(op: (B,A)=> B): B = l match {
      case Cons(h,t)=> foldLeft(t)(op(acc,h))(op)
      case Nil() => acc
    }
    def foldRight[A,B](l: List[A])(acc: B)(op: (A,B)=> B): B = l match{
      case Cons(h,t)=> op(h,foldRight(t)(acc)(op))
      case Nil()=> acc

    }
  }



}

object ListsMain extends App {
  import Lists._

  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
//  println(List.sum(l)) // 60
//  import List._
//  import u03.Lists.List
//  println(append(Cons(5, Nil()), l)) // 5,10,20,30
//  println(List.filter[Int](l)(_ >=20)) // 20,30
//  println(List.flatMapFilter(l)(_ >=20)) // 20,30
//
//  val  lst = Cons(10, Cons(20, List.Cons(30, List.Nil())))
//  println(List.drop(lst ,1)) // Cons(20,Cons(30, Nil()))
//    println(List.drop(lst ,2)) // Cons(30, Nil())
//  println(List.drop(lst ,5)) // Nil()
//  println(List.flatMap(lst)(v => Cons(v+1, Nil()))) // Cons(11,Cons(21,Cons(31,Nil())))
//  println(List.flatMap(lst)(v => Cons(v+1, Cons(v+2, Nil()))))// Cons(11,Cons(12,Cons(21,Cons(22,Cons(31,Cons(32,Nil()))))))
//  println(List.max(lst))
// println(List.flatMapMap(lst)(v => Cons(v+1, Cons(v+2, Nil()))))

  val lst2 = Cons(3,Cons(7,Cons(1,Cons(5, Nil()))))



  println(List.foldLeft(lst2)(0)(_-_)) //  -16
  // foldRight(lst)(0)(_-_) //  -8
}