package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u03.Lists.List
import u03.Lists.List._
import u03.Streams.Stream

class StreamTest {

  @Test
  def dropTest(): Unit = {
    val s = Stream.take(Stream.iterate (0)(_+1))(10)
    assertEquals(Cons(6,Cons(7,Cons(8,Cons(9,Nil())))),Stream.toList(Stream.drop(s)(6)))
  }

  @Test
  def testConstant(): Unit = {
    assertEquals(Cons("x",Cons("x",Cons("x",Cons("x",Cons("x",Nil()))))),Stream.toList(Stream.take(Stream.constant("x"))(5)))
  }
  @Test
  def testFibonacci(): Unit={
    val  fibs: Stream[Int] = Stream.fibonacci()
    assertEquals(Cons(0,Cons(1,Cons(1,Cons(2,Cons(3,Cons(5,Cons(8,Cons(13,Nil())))))))),Stream.toList(Stream.take(fibs)(8)))
  }
}
