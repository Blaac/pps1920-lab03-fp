package u03

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Modules.Person.{Student, Teacher}
import u03.Lists.List
import u03.Lists.List._

class ListTest {


  @Test
  def testFoldLeft(): Unit ={
    val lst2 = Cons(3,Cons(7,Cons(1,Cons(5, Nil()))))

    assertEquals(-16,List.foldLeft(lst2)(0)(_-_))
    // foldRight(lst)(0)(_-_) //  -8)
  }

  @Test
  def testFoldRight(): Unit ={
    val lst2 = Cons(3,Cons(7,Cons(1,Cons(5, Nil()))))

    assertEquals(-8,List.foldRight(lst2)(0)(_-_))

  }


  @Test def testDrop(){
    val  lst = Cons(10, Cons(20, Cons(30, Nil())))

    assertEquals(Cons(20,Cons(30, Nil())), drop(lst ,1))

    assertEquals(Cons(30, Nil()), drop(lst ,2))

    assertEquals(Nil(), drop(lst ,5))
  }




  @Test def testFlatMap(){
    val  lst = Cons(10, Cons(20, List.Cons(30, List.Nil())))

    assertEquals(Cons(11, Cons(21, List.Cons(31, List.Nil()))), flatMap(lst)(v => Cons(v+1, Nil())))
    assertEquals(Cons(11,Cons(12,Cons(21,Cons(22,Cons(31,Cons(32,Nil())))))), flatMap(lst)(v => Cons(v+1, Cons(v+2, Nil()))))
  }


  @Test
  def testMax(): Unit ={
    val lst = Cons(10, Cons(25, Cons(20, Nil())))
    assertEquals(Some(25),max(lst))
    assertEquals(None,max(Nil()))
  }

  @Test
  def testCourseL(): Unit ={

    assertEquals(Cons("oop",Nil()),PersonList.getCourses(Cons(Student("carlo",2020),Cons(Teacher("Pippo","oop"),List.Nil()))))
  }

}